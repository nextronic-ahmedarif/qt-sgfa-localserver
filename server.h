#ifndef SERVER_H
#define SERVER_H
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QThread>
#include <QtNetwork>
#include <iostream>


#include <QByteArray>
#include <QTextStream>
#include <QTimer>
#include<QPrinter>
#include<QPainter>
#include <QDateTime>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QFile>
#include <QIODevice>
#include <QTextStream>

using namespace std;


class server : public QObject
{
    Q_OBJECT

    struct DesktopTempalte{
        QString id;
        QString name;
        QString console_id;
        QString display_id;
        QString active;
    };

    struct ServiceTempalte{
        QString id;
        QString name;
        int min = 0;
        int max = 10;
        int entringQueue;
        int outputQueue;
        void init(){
            entringQueue = min;
            outputQueue = min;
        }
        int Queuers(){
            return entringQueue - outputQueue;
        }
        void setMin(int min){this->min = min;}
        void setMax(int max){this->max = max;}
        int getNextEntringQueue(){
            entringQueue++;
            if(entringQueue == max){
                int diff = entringQueue - outputQueue;
                outputQueue = min - diff;
                entringQueue = min;
            }
            return entringQueue;
        }
        int getNextOutputQueue(){
            if(outputQueue<entringQueue)
                outputQueue++;
            if(outputQueue<min)
                return max -(min - outputQueue);
            if(outputQueue == max)
                outputQueue =min;
            return outputQueue;
        }
    };
    QList<ServiceTempalte> services;
    QList<DesktopTempalte> Desktops;
    QMap<QString,QString> Service_meet_Desktop;
    QUdpSocket *udpSocket4;
    QUdpSocket *udpSocket6;
private slots:
    void processPendingDatagrams();

public:
    server();
};

#endif // SERVER_H
