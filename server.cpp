#include "server.h"

server::server()
{
    std::cout<<"test test"<<std::endl;

    //==================== UDP connexion setup
    QHostAddress groupAddress4(QString("239.255.43.21"));
    QHostAddress groupAddress6(QStringLiteral("ff12::2115"));
    udpSocket4 = new QUdpSocket(this);
    udpSocket6 = new QUdpSocket(this);
    udpSocket4->bind(QHostAddress::AnyIPv4, 45454, QUdpSocket::ShareAddress);
    udpSocket4->joinMulticastGroup(groupAddress4);

    if (!udpSocket6->bind(QHostAddress::AnyIPv6, 45454, QUdpSocket::ShareAddress) ||
            !udpSocket6->joinMulticastGroup(groupAddress6))
        qDebug()<<tr("Listening for multicast messages on IPv4 only");
    connect(udpSocket4, SIGNAL(readyRead()),
            this, SLOT(processPendingDatagrams()));
    connect(udpSocket6, &QUdpSocket::readyRead, this, &server::processPendingDatagrams);
   // connect(this, SIGNAL(clicked()), this, SLOT(close()));
    return;
    QFile file("C:\\agency.json");
    if(!file.open(QFile::ReadOnly)){
        qDebug()<< "Error, Cannot open the file.";
        //return false;
    }
    QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());
    file.close();
    QJsonObject root = jsonDoc.object();
    //    qDebug()<< jsonDoc.object().value("items").toArray()[0].toObject().value("contentDetails").toObject().value("duration").toString();
    QJsonArray Jdesktops = root.value("Desktops").toArray();
    QJsonArray Jservices = root.value("Services").toArray();
    QJsonArray JService_joints = root.value("Service_joints").toArray();

    for (const QJsonValue var :  Jdesktops) {
        DesktopTempalte temp;
        temp.active = var["id"].toString();
        temp.active = var["name"].toString();
        temp.active = var["console_id"].toString();
        temp.active = var["display_id"].toString();
        temp.active = var["active"].toString();
        Desktops.append(temp);
    }
    for (const QJsonValue var :  Jservices) {
        ServiceTempalte servicetemp;
        servicetemp.id = var["id"].toString();
        servicetemp.name = var["name"].toString();
        servicetemp.setMin(var["queue_from"].toString().toInt());
        servicetemp.setMin(var["queue_to"].toString().toInt());
        servicetemp.init();
        services.append(servicetemp);
    }

    for (const QJsonValue var :  JService_joints) {
        Service_meet_Desktop[var["Desktop_id"].toString()] = var["Service_id"].toString();
    }
 qDebug() << services.at(0).name;
 qDebug() << Desktops.at(0).name;
 qDebug() << Service_meet_Desktop["1"];
}
void server::processPendingDatagrams(){
    QByteArray datagram;

    // using QUdpSocket::readDatagram (API since Qt 4)
    while (udpSocket4->hasPendingDatagrams()) {
        datagram.resize(int(udpSocket4->pendingDatagramSize()));
        udpSocket4->readDatagram(datagram.data(), datagram.size());
         qDebug() << tr("Received IPv4 datagram: \"%1\"")
                             .arg(datagram.constData());
    }

    // using QUdpSocket::receiveDatagram (API since Qt 5.8)
    while (udpSocket6->hasPendingDatagrams()) {
        QNetworkDatagram dgram = udpSocket6->receiveDatagram();
         qDebug() << tr("\nReceived IPv6 datagram from [%2]:%3: \"%1\"")
                             .arg(dgram.data().constData(), dgram.senderAddress().toString())
                             .arg(dgram.senderPort());
    }
}
